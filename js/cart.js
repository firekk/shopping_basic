
let price1 = 12.86;
let price2 = 9.99;
let price3 = 15.35;
let price4 = 10.99;
let price5 = 5.30;
let price6 = 11.35;


let add1 = document.getElementById("up1");
let add2=document.getElementById("up2");
let add3=document.getElementById("up3");
let add4=document.getElementById("up4");
let add5=document.getElementById("up5");
let add6=document.getElementById("up6");
let subtract1 = document.getElementById("down1");
let subtract2 = document.getElementById("down2");
let subtract3 = document.getElementById("down3");
let subtract4 = document.getElementById("down4");
let subtract5 = document.getElementById("down5");
let subtract6 = document.getElementById("down6");



	function sum_of_all(){

        let el1=parseFloat(document.getElementById('p1').innerHTML,10);
	let el2=parseFloat(document.getElementById('p2').innerHTML,10);
	let el3=parseFloat(document.getElementById('p3').innerHTML,10);
	let el4=parseFloat(document.getElementById('p4').innerHTML,10);
	let el5=parseFloat(document.getElementById('p5').innerHTML,10);
	let el6=parseFloat(document.getElementById('p6').innerHTML,10);



	let total=el1+el2+el3+el4+el5+el6;
		total=(Math.round(total*100)/100).toFixed(2);

		console.log('total ' + total);
		document.getElementById("sum").innerHTML=total;

	}


add1.addEventListener("click", function () {
	counter('a1', 1);
	product('a1', 'p1', price1);
	sum_of_all();
});

add2.addEventListener("click", function () {
	counter('a2', 1);
	product('a2', 'p2', price2);
	sum_of_all();
});

add3.addEventListener("click", function () {
	counter('a3', 1);
	product('a3', 'p3', price3);
	sum_of_all();
});

add4.addEventListener("click", function () {
	counter('a4', 1);
	product('a4', 'p4', price4);
	sum_of_all();
});

add5.addEventListener("click", function () {
	counter('a5', 1);
	product('a5', 'p5', price5);
	sum_of_all();
});

add6.addEventListener("click", function () {
	counter('a6', 1);
	product('a6', 'p6', price6);
	sum_of_all();
});

subtract1.addEventListener("click", function () {
	counter('a1', -1);
	product('a1', 'p1', price1);
	sum_of_all();
});

subtract2.addEventListener("click", function () {
	counter('a2', -1);
	product('a2', 'p2', price2);
	sum_of_all();
});
subtract3.addEventListener("click", function () {
	counter('a3', -1);
	product('a3', 'p3', price3);
	sum_of_all();
});
subtract4.addEventListener("click", function () {
	counter('a4', -1);
	product('a4', 'p4', price4);
	sum_of_all();
});

subtract5.addEventListener("click", function () {
	counter('a5', -1);
	product('a5', 'p5', price5);
	sum_of_all();
});

subtract6.addEventListener("click", function () {
	counter('a6', -1);
	product('a6', 'p6', price6);
	sum_of_all();
});





function counter(a, val) {

let count = document.getElementById(a).value;
let current_count = parseFloat(count, 10) + val;


	if (current_count < 0) {

		current_count = 0;
	}

	document.getElementById(a).value = current_count;

	return current_count;
};







function product(product_amount, product_total, price) {

let number_of_products = document.getElementById(product_amount).value;

let price_of_products = document.getElementById(product_total);
	price_of_products.innerHTML = number_of_products * price;

};
